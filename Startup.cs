﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BursRehberim.Startup))]
namespace BursRehberim
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
